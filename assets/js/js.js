$(document).ready(function () {
    "use strict"

    // Navigation Submenu
    $("li.menu-item").on("mouseenter", function () {
        $(this).find("ul.sub-menu").addClass("open");
    });
    $("li.menu-item").on("mouseleave", function () {
        $(this).find("ul.sub-menu").removeClass("open");
    });

    // Responsive Navigation Toggle
    $("#menu-toggle").on("click", function () {
        $(this).find("i").toggleClass('icofont-navigation-menu icofont-close');
        $("#site-navigation").toggleClass("open");
    });
});