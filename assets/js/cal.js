// Your Client ID can be retrieved from your project in the Google
// Developer Console, https://console.developers.google.com
var API_KEY = 'AIzaSyDIYlL1tLgB--B0SokKIvVPykkLeON5i-E';
var CAL_ID = 'qv4g1ebj24dfn08k09ommbd0c0@group.calendar.google.com';
var API_URL = 'https://www.googleapis.com/calendar/v3/calendars/' + CAL_ID + '/events';
var TS_FORMAT = 'YYYY-MM-DDTHH:mm:ssZ';
var m  = 0;
var calEventsTemplate;

function getUpcomingEvents() {
    var timeMin = moment().add(m, 'month').startOf('month').startOf('day').format(TS_FORMAT);
    var timeMax = moment().add(m, 'month').endOf('month').endOf('day').format(TS_FORMAT);
    var month = moment().add(m, 'month').format('MMM YYYY');
    var template = Handlebars.compile($('#month-template').html());
    $('#month').html(template( { month: month } ));
    getEvents(timeMin, timeMax);
}

function getEvents(timeMin, timeMax) {
    // $('#cal-events').html(calEventsTemplate(
    //     {
    //         hasItems: false,
    //         hasNoItems: false,
    //         loading: true
    //     }
    // ));
    var params = {
        timeMax: timeMax,
        timeMin: timeMin,
        orderBy: 'startTime',
        singleEvents: true,
        key: API_KEY
    };
    $.get(API_URL, params)
        .done(function(data) {
            processEvents(data.items);
        })
        .fail(function() {
            processEvents([])
        });
}

function processEvents(data) {
    var events = {};
    for(var i = 0; i < data.length; i++) {
        var e = {};
        var day = data[i].start.dateTime ? moment(data[i].start.dateTime).format('DD') : moment(data[i].start.date).format('DD');
        var dow = data[i].start.dateTime ? moment(data[i].start.dateTime).format('ddd') : moment(data[i].start.date).format('ddd');
        var time = data[i].start.dateTime ? moment(data[i].start.dateTime).format('h:mm A') : 'All Day';
        e.summary = data[i].summary;
        e.time = time;
        if(events.hasOwnProperty(day)) {
            events[day].items.push(e);
        }
        else {
            events[day] = { items: [e], dow: dow, day: day }
        }
    }
    var arr = $.map(events, function(el) { return el });
    arr.sort(function(a, b) {
        return parseInt(a.day) - parseInt(b.day);
    });
    $('#cal-events').html(calEventsTemplate(
        {
            items: arr,
            hasItems: arr.length > 0 ? true : false,
            hasNoItems: arr.length == 0 ? true : false,
            loading: false
        }
    ));
}

$(document).ready(function () {
    calEventsTemplate = Handlebars.compile($('#calendar-template').html());
    getUpcomingEvents();
    $('.previous-month a').on('click', function() {
        m--;
        getUpcomingEvents();
    });
    $('.next-month a').on('click', function() {
        m++;
        getUpcomingEvents();
    });
    $('.month').on('click', function() {
        if(m !== 0) {
            m = 0;
            getUpcomingEvents();
        }
    });
});