var audioPlayer;

$(document).ready(function () {
    audioPlayer = $("#player");
    $('#sermons').easyPaginate({
        paginateElement: '.featured-article-wrapper',
        elementsPerPage: 5,
        effect: 'climb',
        firstButton: false,
        lastButton: false,
        hashPage: 'page',
        prevButtonText: '<i class="icofont icofont-simple-left"></i>',
        nextButtonText: '<i class="icofont icofont-simple-right"></i>'
    });
    $('.easyPaginateNav a').click(function() {
        $("html, body").animate({ scrollTop: 0 });
    });
    $('#audioPlayerModal').on('hidden.bs.modal', function () {
        audioPlayer[0].pause();
    })
});

function playAudio(imgUrl, audioUrl) {
    audioPlayer.attr("src", audioUrl);
    $("#audioPlayerImage").attr("src", imgUrl+'?w=800');

    $('#audioPlayerModal').modal({
        show: true
    })

    audioPlayer.load();
    audioPlayer[0].play();
}