$(document).ready(function () {
    evalStatus();
    evalChildren();

    $('#status-select').change(function() {
        evalStatus();
    });


    $('#num-children-select').change(function() {
       evalChildren();
    });
});

function evalStatus() {
    var status = $('#status-select').val();
    if(status === 'Married') {
        $('#spouse-section').show();
    }
    else {
        $('#spouse-section').hide();
        $('#spouse-first-name').val('');
        $('#spouse-last-name').val('');
        $('#spouse-shirt-size').val('');
    }
}

function evalChildren() {
    var numChildren = $('#num-children-select').val();
    for(var i = 1; i <= 5; i++) {
        if(i > numChildren) {
            $('#child-' + i).hide();

            $('#child-' + i + '-first-name').val('');
            $('#child-' + i + '-last-name').val('');
            $('#child-' + i + '-shirt-size').val('');
        }
        else {
            $('#child-' + i).show();
        }
    }
}

jQuery.validator.addMethod('phoneUS', function(phone_number, element) {
    phone_number = phone_number.replace(/\s+/g, '');
    return this.optional(element) || phone_number.length > 9 &&
        phone_number.match(/^(1-?)?(\([2-9]\d{2}\)|[2-9]\d{2})-?[2-9]\d{2}-?\d{4}$/);
}, 'Please enter a valid phone number.');

$("#reg-form").validate();