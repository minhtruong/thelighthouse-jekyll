$(function () {
    var cloudName = "lighthouse-dfw";
    var gallery = $("#gallery");
    var tag = $("#galleryTag").data("tag");
    var cl = cloudinary.Cloudinary.new({
        cloud_name: cloudName
    });
    var url = cl.url(tag + '.json', {
        type: "list"
    });
    console.log(url);
    $.get(url, function (data) {
        console.log(data);
        console.log(gallery);
        for (var i = 0; i < data.resources.length; i++) {
            var img = data.resources[i];
            var url = 'http://res.cloudinary.com/' + cloudName + '/image/upload/' + img.public_id + '.' + img.format;
            var thumbUrl = 'http://res.cloudinary.com/' + cloudName + '/image/upload/h_400/' + img.public_id + '.' + img.format;
            gallery.append('<div class="col-md-3 col-sm-4 col-xs-12"><a href="' + url + '" data-lightbox="group"><div class="thumb" style="background-image:url(\'' + thumbUrl + '\')"></div></a></div>');
            // gallery.append('<div class="col-md-3 col-sm-4 col-xs-12"><a href="' + url + '" data-lightbox="group"><img class="img-responsive" src="' + thumbUrl + '"/></div></a></div>');
        }
    });
});

